#!/usr/bin/env python
# coding: utf-8

from flask_frozen import Freezer
from app import app
import os

app.config["FREEZER_IGNORE_MIMETYPE_WARNINGS"] = True
#app.config["FREEZER_SKIP_EXISTING"] = True
freezer = Freezer(app)

if os.environ.get("CI"):
    app.config["FREEZER_BASE_URL"] = "/kansensp_db/"
    app.config["FREEZER_DESTINATION"] = "public"

if __name__ == "__main__":
    freezer.freeze()
    #freezer.run(debug=True)
