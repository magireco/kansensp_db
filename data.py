#!/usr/bin/env python
# coding: utf-8

import json
import pandas as pd

data = {}


def read_chara():
    filename = "json/CharaInfo.json"
    path = "characterInfo"
    with open(filename) as f:
        j = json.load(f)[path]

    cp = j[0].copy()
    top_keys = []
    for key in ["name", "kana", "comment"]:
        if key in cp.keys():
            del cp[key]
            top_keys.append(key)
    cols = top_keys + sorted(cp.keys())

    df = pd.DataFrame(j, columns=cols)
    df.set_index(["id"], inplace=True)
    #df.to_excel("xlsx/CharaInfo.xlsx")
    return df


def read_battle():
    filename = "json/battle.json"
    with open(filename) as f:
        j = json.load(f)["index_0"]["extra_requests"]

    df = pd.DataFrame(j)
    df.set_index(["id"], inplace=True)
    return df


def read_json(dataname):
    filename = "json/{0}.json".format(dataname)
    path = (dataname[0]).lower() + dataname[1:] + "Data"
    with open(filename) as f:
        try:
            j = json.load(f)[path]
        except Exception as e:
            print "fail to load {0}/{1}".format(filename, path)
            print e
            raise e

    print dataname
    cp = j[0].copy()
    top_keys = []
    for key in ["name", "kana", "comment"]:
        if key in cp.keys():
            del cp[key]
            top_keys.append(key)
    cols = top_keys + sorted(cp.keys())

    df = pd.DataFrame(j, columns=cols)
    df.set_index(["id"], inplace=True)
    #df.to_excel("xlsx/{0}.xlsx".format(dataname))
    return df

for d in ["Ability", "Armor", "CharaFlavortext", "DiamondBonus",
          "Dna", "Enchant", "Enemy", "EnemyAction", "EnemySkill", "Item",
          "ItemMake", "LoginBonus", "Mission", "Quest",# "QuestBattle",
          "QuestEnemy", # "Questpart",
          "Skill", "Weapon"
          ]:
    # ignore: StoryCharacter, UserExperience
    data[d] = read_json(d)
data["Chara"] = read_chara()
data["Battle"] = read_battle()


if __name__ == "__main__":
    pass
