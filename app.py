#!/usr/bin/env python
# coding: utf-8

from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import Subgroup, View, Navbar

app = Flask(__name__)
Bootstrap(app)
app.debug = True

nav = Nav()

nav.register_element('top', Navbar(
    View(u'感染×少女SPデータ', 'index'),
    Subgroup(
        u'キャラ',
        View(u'適当な一覧', 'characters')
    ),
    Subgroup(
        u'アイテム',
        View(u'武器', 'weapons'),
        View(u'防具', 'armors'),
    ),
    Subgroup(
        u'クエスト',
        View(u'クエスト', 'quests'),
        View(u'クエスト敵', 'questenemies'),
        View(u'Exバトル', 'exbattles'),
    ),
    View('DNA', 'dnas'),
))
nav.init_app(app)

from data import data

effects = {
    "d_slack": u"d_slack{0}",
    "d_blind": u"暗闇耐性{0}",
    "d_seal": u"d_seal{0}",
    "d_dizzy": u"d_dizzy{0}",
    "d_infection": u"d_infection{0}",
    "d_sleep": u"d_sleep{0}",
    "d_paralysis": u"麻痺耐性{0}",
    "d_poison": u"毒耐性{0}",
    "counter": u"反撃{0}",
    "guard": u"防御{0}",
    "avoidance": u"回避{0}",
    "hit": u"命中{0}",
    "critical": u"強撃{0}",
    "p_recovery_rate": "p_recovery_rate{0}",
    "p_recovery": "p_recovery{0}",
    "p_defC": "p_defC{0}",
    "terms_hp": "terms_hp{0}",
    "terms_type": "terms_type{0}",
    "hp": "HP{0}",
    "damP": u"物攻{0}",
    "damC": u"化攻{0}",
    "recovery": "recovery{0}",
    "recovery_rate": "recovery_rate{0}",
    "p_hp": u"最大HP{0}%",
    "p_damP": u"物攻{0}%",
    "p_damC": u"化攻{0}%",
    "p_defP": u"物防{0}%",
    "p_defC": u"化防{0}%",
}


def dna_effect(dna):
    values = []
    for ef in effects.keys():
        if dna[ef] > 0:
            values.append(effects[ef].format(dna[ef]))
    return u"／".join(values)

app.add_template_global(data, name="data")
app.add_template_global(type, name="type")
app.add_template_global(["0", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"], name="rank")
app.add_template_global(["?", "N", "R", "SR", "SSR", "UR", "LR"], name="rare")
app.add_template_global(["?", u"剣道部", u"射撃部", u"武術部", u"化学部", u"音楽部"], name="club")
app.add_template_global(dna_effect, name="dna_effect")


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/Chara/<int:id_>.html")
def chara(id_):
    print "Chara", id_
    return render_template("Chara.html", id_=id_)


@app.route("/Characters.html")
def characters():
    return render_template("Characters.html")


@app.route("/Weapons.html")
def weapons():
    return render_template("Weapons.html")


@app.route("/Armors.html")
def armors():
    return render_template("Armors.html")


@app.route("/Quests.html")
def quests():
    return render_template("Quests.html")


@app.route("/QuestEnemis.html")
def questenemies():
    return render_template("QuestEnemies.html")


@app.route("/Dnas.html")
def dnas():
    return render_template("Dnas.html")


@app.route("/ExBattles.html")
def exbattles():
    return render_template("ExBattles.html")


@app.route("/Enemy/<int:eid>.html")
def enemy(eid):
    try:
        html = render_template("Enemy.html", eid=eid)
        return html
    except:
        return "ERROR"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
